package cl.gechs.cursojava.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 *
 * @author Daniel Canales <carlos.soza at gmail dot com>
 */
@Entity
@Table(name = "pregunta", uniqueConstraints = @UniqueConstraint(columnNames = "idPregunta"))
public class encPreg implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    private String pregunta;
    
    private pregAlt PregAlt;
    

    @Id
    @GeneratedValue
    @Basic(optional = false)
    @Column(name = "idPregunta")
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    @Basic(optional = false)
    @Column(name = "pregunta")
    public String getPregunta() {
        return pregunta;
    }
    public void setPregunta(String pregunta) {
        this.pregunta = pregunta;
    }
    
    @Basic(optional = false)
    @JoinColumn(name = "idAlternativa", referencedColumnName = "idAlternativa")
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    public pregAlt getPregAlt() {
        return PregAlt;
    }
    public void setPregAlt(pregAlt PregAlt) {
        this.PregAlt = PregAlt;
    }
    
    @Override
    public String toString() {
        return id + " | " + pregunta;
    }
}
