package cl.gechs.cursojava.survey.webservices;

import cl.gechs.cursojava.survey.webservices.data.UserServiceRequestType;
import cl.gechs.cursojava.survey.webservices.data.UserServiceResponseType;
import javax.jws.WebMethod;
import javax.jws.WebService;

/**
 *
 * @author Carlos Soza Canales <carlos.soza at gmail dot com>
 */
@WebService(targetNamespace = "http://www.gechs.cl/webservices/ws-user")
public interface UserWSI {

    @WebMethod
    public UserServiceResponseType userByIdRequest(UserServiceRequestType request);

    @WebMethod
    public UserServiceResponseType userListRequest();
}
