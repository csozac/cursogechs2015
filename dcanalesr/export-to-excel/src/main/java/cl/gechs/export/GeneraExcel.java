 package cl.gechs.export; 
 import java.io.FileOutputStream; 
 import org.apache.log4j.Logger; 
 import org.apache.poi.hssf.usermodel.HSSFWorkbook; 
 import org.apache.poi.ss.usermodel.Row; 
 import org.apache.poi.ss.usermodel.Sheet; 
 import org.apache.poi.ss.usermodel.Workbook;
/** * * @author Carlos Soza C <carlos.soza at profondos.com> */ 
 public class GeneraExcel {
    private static final Logger LOGGER = Logger.getLogger(GeneraExcel.class);
    public void generaExcel(String ﬁlename) {        
        try {            
            Workbook workbook = new HSSFWorkbook();            
            Sheet sheet = workbook.createSheet("Alumnos");
            Row rowhead = sheet.createRow((short) 0);            
            rowhead.createCell(0).setCellValue("Nombre");            
            rowhead.createCell(1).setCellValue("Rut");            
            rowhead.createCell(2).setCellValue("Correo");
            Row row = sheet.createRow((short) 1);            
            row.createCell(0).setCellValue("Daniel Canales Rivas");            
            row.createCell(1).setCellValue("15566778-8");            
            row.createCell(2).setCellValue("dcanales@gmail.com");
            FileOutputStream ﬁleOut = new FileOutputStream(ﬁlename);            
            workbook.write(ﬁleOut);            
            ﬁleOut.close();        
        } catch (Exception ex) {            
            LOGGER.error("Error al generar el archivo: " + ex.getMessage(), ex);        
        }    
    } 
}