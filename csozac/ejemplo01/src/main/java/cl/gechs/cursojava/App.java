package cl.gechs.cursojava;

/**
 * Hello world!
 *
 */
public class App {

    public static void main(String[] args) {
        System.out.println("Hello World!");
        App app = new App();
        int r=4;
        r++;
        int suma = app.suma(3, 4);
        System.out.println("Suma: " + suma);
    }

    public int suma(int a, int b) {
        return a + b;
    }
}
