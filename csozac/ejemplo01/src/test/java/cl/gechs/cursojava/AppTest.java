package cl.gechs.cursojava;

import org.junit.Assert;

/**
 * Unit test for simple App.
 */
public class AppTest {

    @org.junit.Test
    public void testSuma() {
        App app = new App();
        Assert.assertEquals(7, app.suma(3, 3));

    }
}
