package cl.gechs.export;

import org.apache.log4j.Logger;

/**
 *
 * @author Carlos Soza C <carlos.soza at profondos.com>
 */
public class ExportToExcel {

    private static final Logger LOGGER = Logger.getLogger(ExportToExcel.class);

    /**
     * Este método es el main del proyecto
     * @param args paramteros de entrada desde consola
     */
    public static void main(String[] args) {
        LOGGER.debug("Generando Archivo");
        GeneraExcel generaExcel = new GeneraExcel();
        // Usar Path de archivo de sus máquinas
        generaExcel.generaExcel("/Users/csozac/Downloads/Test.xls");
        LOGGER.debug("Archivo Generado con Éxito");
    }
}
