package  cl.gechs.cursojava.tallermecanico;

/**
 *
 * @author csozac
 */
public class Persona
{

    private String nombre;
    private String rut;
    private int disponibleCC;
    private Auto auto;
    private String diagnostico;

    /**
     * Constructor Vacio
     */
    public Persona()
    {
    }

    /**
     * Contructor para setear los valores de los atributos de los objetos instanciados de la clase Persona.
     * @param nombre
     * @param rut
     * @param disponibleCC
     * @param auto
     */
    public Persona(String nombre, String rut, int disponibleCC, Auto auto)
    {
        this.nombre = nombre;
        this.rut = rut;
        this.disponibleCC = disponibleCC;
        this.auto = auto;
    }

    public Auto getAuto()
    {
        return auto;
    }

    public void setAuto(Auto auto)
    {
        this.auto = auto;
    }

    public int getDisponibleCC()
    {
        return disponibleCC;
    }

    public void setDisponibleCC(int disponibleCC)
    {
        this.disponibleCC = disponibleCC;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public String getRut()
    {
        return rut;
    }

    public void setRut(String rut)
    {
        this.rut = rut;
    }

    public String getDiagnostico()
    {
        return diagnostico;
    }

    public void setDiagnostico(String diagnostico)
    {
        this.diagnostico = diagnostico;
    }

    /**
     * 
     * @return String con los datos de la Persona y su auto.
     */
    public String mostrarDatos()
    {
        String resp = "";
        resp += "Nombre: " + nombre + "\n";
        resp += "Rut: " + rut + "\n";
        resp += "DisponibleCC: " + disponibleCC + "\n";
        resp += "Auto Marca: " + auto.getMarca() + "\n";
        resp += "Auto Modelo: " + auto.getModelo() + "\n";
        resp += "Auto Color: " + auto.getColor() + "\n";
        resp += "Auto N Puertas: " + auto.getNumeroDePuertas() + "\n";
        resp += "Auto KM: " + auto.getKilometraje() + "\n";
        resp += "Auto Nivel Agua: " + auto.getNivelDeAgua() + "\n";
        resp += "Auto Nivel Aceite: " + auto.getNivelDeAceite() + "\n";
        resp += "Diagnóstico: " + diagnostico + "\n";
        resp += "\n";
        return resp;
    }
}
