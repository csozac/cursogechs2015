/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.gechs.export;

import org.apache.log4j.Logger;

/**
 *
 * @author Cliente
 */
public class ExportToExcel {

	private static final Logger LOGGER = Logger.getLogger(ExportToExcel.class);

	/**
	 * @param args the command line arguments
	 */
	public static void main(String[] args) {
		LOGGER.info("Generando Archivo");
		GeneraExcel generaExcel = new GeneraExcel();
// Usar Path de archivo de sus máquinas
		generaExcel.generaExcel("/Users/Cliente/Downloads/Test.xls");
		LOGGER.info("Archivo Generado con Éxito");
	}
}
