package cl.gechs.cursojava.dao;

import cl.gechs.cursojava.model.Contacto;
import java.util.List;
import javax.persistence.Query;
import org.apache.log4j.Logger;

/**
 *
 * @author Carlos Soza Canales <carlos.soza at gmail dot com>
 */
@javax.ejb.Stateless
@javax.ejb.LocalBean
public class ContactoDAO extends BaseDAO<Contacto> {

    private static final Logger LOGGER = Logger.getLogger(ContactoDAO.class);

    @Override
    public Class<Contacto> getObjectClass() {
        return Contacto.class;
    }

    public List<Contacto> getContactos() {
        Query query = this.getEntityManager().createQuery("FROM Contacto");
        try {
            return (List<Contacto>) query.getResultList();
        } catch (javax.persistence.NoResultException ex) {
            return null;
        } catch (Exception ex) {
            LOGGER.error("ERROR DAO: ", ex);
            return null;
        }
    }

    public Contacto getContacto(long idContacto) {
        try {
            return this.getById(idContacto);
        } catch (Exception ex) {
            LOGGER.error("Error al recuperar Usuarios.", ex);
            return null;
        }
    }

	public List<Contacto> getContactosPorEmpresa(String empresa) {
		Query query = this.getEntityManager().createQuery("FROM Contacto contacto where contacto.empresa=:empresa");
		query.setParameter("empresa", empresa);
		try {
			return (List<Contacto>) query.getResultList();
		} catch (javax.persistence.NoResultException ex) {
			return null;
		} catch (Exception ex) {
			LOGGER.error("ERROR DAO: ", ex);
			return null;
		}
	}

}
