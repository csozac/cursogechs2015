package cl.gechs.cursojava.contactos.webservices;

import cl.gechs.cursojava.contactos.webservices.data.ContactoServiceRequestType;
import cl.gechs.cursojava.contactos.webservices.data.ContactoServiceResponseType;
import cl.gechs.cursojava.contactos.webservices.data.ContactoServiceResultType;
import cl.gechs.cursojava.model.Contacto;
import cl.gechs.cursojava.service.ContactoService;
import cl.gechs.cursojava.service.utils.ServicesDirectory;
import java.util.List;
import javax.jws.WebService;

/**
 *
 * @author Carlos Soza Canales <carlos.soza at gmail dot com>
 */
@WebService(
        portName = "ContactoWSPort",
        serviceName = "ContactoWS",
        targetNamespace = "http://www.gechs.cl/webservices/ws-contacto",
        endpointInterface = "cl.gechs.cursojava.contactos.webservices.ContactoWSI")
public class ContactoWS implements ContactoWSI {

    private static final ContactoService CONTACTOS_SERVICE;

    static {
        CONTACTOS_SERVICE = ServicesDirectory.getContactoService();
    }

    @Override
    public ContactoServiceResponseType contactoByIdRequest(ContactoServiceRequestType request) {

        Contacto contacto = CONTACTOS_SERVICE.getContacto(Long.parseLong(request.getContactoId()));
        ContactoServiceResponseType response = new ContactoServiceResponseType();
        response.setResult(ContactoServiceResultType.OK);
        response.setDescription(contacto.toString());
        return response;
    }

    @Override
    public ContactoServiceResponseType contactoListRequest() {
        List<Contacto> contactos = CONTACTOS_SERVICE.getContactos();
        String resp = "";
        for (Contacto c : contactos) {
            resp += c + "\n";
        }
        ContactoServiceResponseType response = new ContactoServiceResponseType();
        response.setResult(ContactoServiceResultType.OK);
        response.setDescription(resp);
        return response;
    }
}
