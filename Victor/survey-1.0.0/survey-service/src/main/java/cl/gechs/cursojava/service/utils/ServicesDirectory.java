package cl.gechs.cursojava.service.utils;

import cl.gechs.cursojava.service.UsersService;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import org.apache.log4j.Logger;

/**
 *
 * @author David Gutiérrez <david.gutierrez at profondos.com>
 */
public class ServicesDirectory {

    private static final Logger LOGGER = Logger.getLogger(ServicesDirectory.class);
    // services
    private static transient UsersService userService;

    private ServicesDirectory() {
    }

    public static UsersService getUserService() {
        try {
            if (userService == null) {
                userService = (UsersService) InitialContext
                        .doLookup("java:global/survey-ear/survey-service/UsersService!cl.gechs.cursojava.service.UsersService");
            }
        } catch (NamingException ex) {
            LOGGER.warn("Error injecting services: " + ex.getMessage(), ex);
        }

        try {
            if (userService == null) {
                userService = (UsersService) InitialContext
                        .doLookup("java:global/survey-service/UsersService!cl.gechs.cursojava.service.UsersService");
            }
        } catch (NamingException ex) {
            LOGGER.warn("Error injecting services: " + ex.getMessage(), ex);
        }

        return userService;
    }
}
