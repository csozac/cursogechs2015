package cl.gechs.cursojava.rest;

import cl.gechs.cursojava.model.User;
import cl.gechs.cursojava.service.UsersService;
import cl.gechs.cursojava.service.utils.ServicesDirectory;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.util.List;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import org.apache.log4j.Logger;

/**
 *
 * @author Carlos Soza C <carlos.soza at profondos.com>
 * @author David Gutierrez <david.gutierrez at profondos.com>
 */
@Path("/surveys")
public class SurveyRestService {

    private static final Logger LOGGER = Logger.getLogger(SurveyRestService.class);
     private static final Gson GSON = new GsonBuilder().setDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz").create();
    private static final UsersService USERS_SERVICE;

    static {
        USERS_SERVICE = ServicesDirectory.getUserService();
    }

    @GET
    @Path("users")
    @Produces({"application/json"})
    public String getNewUnderlyingAssets() {
        List<User> users = USERS_SERVICE.getUsers();
        return GSON.toJson(users);
    }

}
