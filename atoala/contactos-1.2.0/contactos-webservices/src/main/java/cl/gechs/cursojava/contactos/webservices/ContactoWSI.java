package cl.gechs.cursojava.contactos.webservices;


import cl.gechs.cursojava.contactos.webservices.data.ContactoServiceRequestType;
import cl.gechs.cursojava.contactos.webservices.data.ContactoServiceResponseType;
import javax.jws.WebMethod;
import javax.jws.WebService;

/**
 *
 * @author Carlos Soza Canales <carlos.soza at gmail dot com>
 */
@WebService(targetNamespace = "http://www.gechs.cl/webservices/ws-contacto")
public interface ContactoWSI {

    @WebMethod
    public ContactoServiceResponseType contactoByIdRequest(ContactoServiceRequestType request);

    @WebMethod
    public ContactoServiceResponseType contactoListRequest();
}
