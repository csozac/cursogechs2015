/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.gechs.export;

import org.apache.log4j.Logger;

/**
 *
 * @author Kuvasz
 */
public class ExportToExcel {

    private static final Logger LOGGER = Logger.getLogger(ExportToExcel.class);
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        LOGGER.debug("Generando Archivo");
        GeneraExcel generaExcel = new GeneraExcel();
        // Usar Path de archivo de sus máquinas
        generaExcel.generaExcel("/Users/Kuvasz/Desktop/Test.xls");
        LOGGER.debug("Archivo Generado con Éxito");
        
        
     }
    
}
