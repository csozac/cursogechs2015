/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.gechs.cursojava.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;


/**
 *
 * @author Alexander Toala
 */

@Entity
@Table(name = "encuesta", uniqueConstraints = @UniqueConstraint(columnNames = "encuesta_id"))
public class Encuesta implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idEncuesta;

    private String nombre;

    private String descripcion;

    private Date fecha;

    @Id
    @GeneratedValue
    @Basic(optional = false)
    @Column(name = "encuesta_id")
    public Long getidEncuesta() {
        return idEncuesta;
    }

    public void setidEncuesta(Long idEncuesta) {
        this.idEncuesta = idEncuesta;
    }

    @Basic(optional = false)
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    @Basic(optional = false)
    @Column(name = "descripcion")
    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
    @Basic(optional = false)
    @Column(name = "fecha")
    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    /*@Basic(optional = false)
    @JoinColumn(name = "id_rol_usuario", referencedColumnName = "id_rol_usuario")
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    public UserRol getUserRol() {
        return userRol;
    }*/

    /*@Override
    public String toString() {
        return id + " | " + name + " | " + email + " | " + userRol.getNombre() + " | " + password;
    }*/
}

