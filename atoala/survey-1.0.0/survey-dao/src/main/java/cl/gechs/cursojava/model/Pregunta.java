/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.gechs.cursojava.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 *
 * @author Alexander Toala
 */

@Entity
@Table(name = "pregunta", uniqueConstraints = @UniqueConstraint(columnNames = "pregunta_id"))
public class Pregunta implements Serializable {

    private static final long serialVersionUID = 2L;

    private Long idPregunta;

    private String pregunta;
    
    private Encuesta encuesta;

    @Id
    @GeneratedValue
    @Basic(optional = false)
    @Column(name = "pregunta_id")
    public Long getidPregunta() {
        return idPregunta;
    }

    public void setidPregunta(Long idPregunta) {
        this.idPregunta = idPregunta;
    }

    @Basic(optional = false)
    @Column(name = "pregunta")
    public String getPregunta() {
        return pregunta;
    }

    public void setPregunta(String pregunta) {
        this.pregunta = pregunta;
    }
    
    @Basic(optional = false)
    @JoinColumn(name = "encuesta_id", referencedColumnName = "encuesta_id")
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    public Encuesta getEncuesta() {
        return encuesta;
    }

    public void setEncuesta(Encuesta encuesta) {
        this.encuesta = encuesta;
    }
    
    /*@Override
    public String toString() {
        return id + " | " + name + " | " + email + " | " + userRol.getNombre() + " | " + password;
    }*/
}


