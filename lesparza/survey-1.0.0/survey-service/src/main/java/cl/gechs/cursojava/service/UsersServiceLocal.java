package cl.gechs.cursojava.service;

import cl.gechs.cursojava.model.User;
import cl.gechs.cursojava.model.UserRol;
import cl.gechs.cursojava.model.UserRolEnum;
import java.util.List;

/**
 *
 * @author Carlos Soza C <carlos.soza at profondos dot com>
 */
@javax.ejb.Local
public interface UsersServiceLocal {

    public void start();

    public void stop();

    public boolean saveUser(User user);

    public boolean updateUser(User user);

    public String generatePassword(Long userId);

    public User getUser(Long userId);

    public List<User> getUsers();

    public List<User> getUsers(UserRolEnum role);

    public List<UserRol> getUserRoles();

    public UserRol getUserRole(UserRolEnum role);
}
