package cl.gechs.cursojava.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 *
 * @author Daniel Canales <carlos.soza at gmail dot com>
 */
@Entity
@Table(name = "alternativa", uniqueConstraints = @UniqueConstraint(columnNames = "idAlternativa"))
public class pregAlt implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    private String alternativa;
   
    @Id
    @GeneratedValue
    @Basic(optional = false)
    @Column(name = "idAlternativa")
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    @Basic(optional = false)
    @Column(name = "alternativa")
    public String getAlternativa() {
        return alternativa;
    }
    public void setAlternativa(String alternativa) {
        this.alternativa = alternativa;
    }
    
        
    @Override
    public String toString() {
        return id + " | " + alternativa;
    }
}
