package cl.gechs.cursojava.dao.jpa;

import cl.gechs.cursojava.model.User;
import cl.gechs.cursojava.model.UserRol;
import cl.gechs.cursojava.model.UserRolEnum;
import java.util.List;
import javax.persistence.Query;
import org.apache.log4j.Logger;

/**
 *
 * @author Carlos Soza Canales <carlos.soza at gmail dot com>
 */
@javax.ejb.Stateless
@javax.ejb.LocalBean
public class UserDAO extends BaseDAO<User> {

    private static final Logger LOGGER = Logger.getLogger(UserDAO.class);

    @Override
    public Class<User> getObjectClass() {
        return User.class;
    }

    public List<UserRol> getUserRoles() {
        Query query = this.getEntityManager().createQuery("FROM UserRol");
        try {
            return (List<UserRol>) query.getResultList();
        } catch (javax.persistence.NoResultException ex) {
            return null;
        } catch (Exception ex) {
            LOGGER.error("ERROR DAO: ", ex);
            return null;
        }
    }

    public List<User> getUsers() {
        Query query = this.getEntityManager().createQuery("FROM User");
        try {
            return (List<User>) query.getResultList();
        } catch (javax.persistence.NoResultException ex) {
            return null;
        } catch (Exception ex) {
            LOGGER.error("ERROR DAO: ", ex);
            return null;
        }
    }

    public List<User> getUsers(UserRolEnum rol) {
        Query query = this.getEntityManager().createQuery("FROM User where user.userRol.nombre=:rol");
        query.setParameter("rol", rol);
        try {
            return (List<User>) query.getResultList();
        } catch (javax.persistence.NoResultException ex) {
            return null;
        } catch (Exception ex) {
            LOGGER.error("ERROR DAO: ", ex);
            return null;
        }
    }

    public User getUser(long idUser) {
        try {
            return this.getById(idUser);
        } catch (Exception ex) {
            LOGGER.error("Error al recuperar Usuarios.", ex);
            return null;
        }
    }

    public User getUserByName(String username) {

        Query query = this.getEntityManager().createQuery("FROM User user WHERE user.username=:username");
        query.setParameter("username", username);
        query.setMaxResults(1);
        try {
            return (User) query.getSingleResult();
        } catch (javax.persistence.NoResultException ex) {
            return null;
        } catch (Exception ex) {
            LOGGER.error("ERROR DAO: ", ex);
            return null;
        }
    }

    public UserRol getUserRolByName(UserRolEnum rol) {

        Query query = this.getEntityManager().createQuery("FROM UserRol rol WHERE rol.nombre=:rol");
        query.setParameter("rol", rol);
        query.setMaxResults(1);
        try {
            return (UserRol) query.getSingleResult();
        } catch (javax.persistence.NoResultException ex) {
            return null;
        } catch (Exception ex) {
            LOGGER.error("ERROR DAO: ", ex);
            return null;
        }
    }
}
