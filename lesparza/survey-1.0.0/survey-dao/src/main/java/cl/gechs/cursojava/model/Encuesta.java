package cl.gechs.cursojava.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 *
 * @author Daniel Canales <carlos.soza at gmail dot com>
 */
@Entity
@Table(name = "encuesta", uniqueConstraints = @UniqueConstraint(columnNames = "idEncuesta"))
public class Encuesta implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    private String nombre;

    private String descripcion;

    private String fecha;
    
    private encPreg EncPreg;

    @Id
    @GeneratedValue
    @Basic(optional = false)
    @Column(name = "idEncuesta")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Basic(optional = false)
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic(optional = false)
    @Column(name = "descripcion")
    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Basic(optional = false)
    @Column(name = "fecha")
    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }
    
    @Basic(optional = false)
    @JoinColumn(name = "idPregunta", referencedColumnName = "idPregunta")
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    public encPreg getEncPreg() {
        return EncPreg;
    }
    public void setEncPreg(encPreg EncPreg) {
        this.EncPreg = EncPreg;
    }

    
    @Override
    public String toString() {
        return id + " | " + nombre + " | " + descripcion + " | " + fecha + " | " + EncPreg;
    }
}
