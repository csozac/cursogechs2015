package cl.gechs.cursojava.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Carlos Soza Canales <carlos.soza at gmail dot com>
 * @param <T>
 */
public abstract class BaseDAO<T> {

    private static final org.apache.log4j.Logger LOGGER = org.apache.log4j.Logger.getLogger(BaseDAO.class);
    @PersistenceContext(unitName = "contactos-pu")
    private EntityManager em;

    public abstract Class<T> getObjectClass();

    public EntityManager getEntityManager() {
        return em;
    }

    public boolean save(T entity) {
        try {
            getEntityManager().persist(entity);
            getEntityManager().flush();
            return true;
        } catch (Exception ex) {
            LOGGER.error("Error saving Entity: " + ex.getMessage(), ex);
        }
        return false;
    }

    public boolean update(T entity) {
        try {
            getEntityManager().merge(entity);
            getEntityManager().flush();
            return true;
        } catch (Exception ex) {
            LOGGER.error("Error updating Entity: " + ex.getMessage(), ex);
        }
        return false;
    }

    public boolean delete(T entity) {
        try {
            Object mentity = getEntityManager().merge(entity);
            getEntityManager().remove(mentity);
            getEntityManager().flush();
            return true;
        } catch (Exception ex) {
            LOGGER.error("Error deleting Entity: " + ex.getMessage(), ex);
        }
        return false;
    }

    public boolean delete(Long id) {
        try {
            Object obj = getEntityManager().find(getObjectClass(), id);
            getEntityManager().remove(obj);
            getEntityManager().flush();
            return true;
        } catch (Exception ex) {
            LOGGER.error("Error deleting Entity: " + ex.getMessage(), ex);
        }
        return false;
    }

    public T getById(Long id) {
        try {
            return getEntityManager().find(getObjectClass(), id);
        } catch (Exception ex) {
            LOGGER.error("Error getting Entity by Id: " + ex.getMessage(), ex);
        }
        return null;
    }

    public T merge(T entity) {
        try {
            Object mentity = getEntityManager().merge(entity);
            getEntityManager().flush();
            return (T) mentity;
        } catch (Exception ex) {
            LOGGER.error("Error Merging Entity by Id: " + ex.getMessage(), ex);
        }
        return null;
    }
}
