package cl.gechs.cursojava.service;

import cl.gechs.cursojava.dao.ContactoDAO;
import cl.gechs.cursojava.model.Contacto;
import java.lang.management.ManagementFactory;
import java.util.List;
import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.management.InstanceAlreadyExistsException;
import javax.management.InstanceNotFoundException;
import javax.management.MBeanRegistrationException;
import javax.management.MBeanServer;
import javax.management.MalformedObjectNameException;
import javax.management.NotCompliantMBeanException;
import javax.management.ObjectName;
import org.apache.log4j.Logger;

/**
 *
 * @author Carlos Soza C <carlos.soza at gmail dot com>
 */
@Startup
@Singleton
@LocalBean
@ConcurrencyManagement(ConcurrencyManagementType.CONTAINER)
public class ContactoService implements ContactoServiceMXBean {

    private static final Logger logger = Logger.getLogger(ContactoService.class);
    //SERVICES

    @EJB
    private ContactoDAO contactoDAO;
    //MBEAN
    private static MBeanServer platformMBeanServer;
    private static ObjectName objectName = null;

    @javax.annotation.PostConstruct
    public void start() {
        logger.info("-------------------------------------------------------------------------");
        logger.info("-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-");
        logger.info("Starting Users Service");

        //register mbean
        try {
            objectName = new ObjectName("contactos:services=" + this.getClass().getName());
            platformMBeanServer = ManagementFactory.getPlatformMBeanServer();
            platformMBeanServer.registerMBean(this, objectName);
        } catch (MalformedObjectNameException | InstanceAlreadyExistsException | MBeanRegistrationException | NotCompliantMBeanException e) {
            throw new IllegalStateException("Problem during registration of Monitoring into JMX:" + e);
        }

        logger.info("-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-");
        logger.info("-------------------------------------------------------------------------");
    }

    @javax.annotation.PreDestroy
    public void stop() {
        logger.info("-------------------------------------------------------------------------");
        logger.info("-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-");
        logger.info("Ending Users Service");

        //unregister mbean
        try {
            platformMBeanServer.unregisterMBean(objectName);
        } catch (InstanceNotFoundException | MBeanRegistrationException e) {
            throw new IllegalStateException("Problem during unregistration of Monitoring into JMX:" + e);
        }

        logger.info("-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-");
        logger.info("-------------------------------------------------------------------------");
    }

    @Lock(LockType.WRITE)
    public boolean saveContacto(Contacto user) {
        try {
            //revisar user id
            Contacto euser = contactoDAO.getContacto(user.getId());
            if (euser == null) {
                if (contactoDAO.save(user)) {
                    return true;
                } else {
                    logger.error("Cannot save User " + user + ". Database error");
                }
            } else {
                logger.error("Cannot save User " + user + ". UserId already registered");
            }
        } catch (Exception ex) {
            logger.error("Cannot save User " + user + ": ", ex);
        }
        return false;
    }

    @Lock(LockType.WRITE)
    public boolean updateContacto(Contacto user) {
        try {
            //check user id
            Contacto euser = contactoDAO.getContacto(user.getId());
            if (euser != null) {
                if (contactoDAO.update(user)) {
                    return true;
                } else {
                    logger.error("Cannot update User " + user + ". Database error");
                }
            } else {
                logger.error("Cannot update User " + user + ". UserId not registered");
            }
        } catch (Exception ex) {
            logger.error("Cannot update User " + user + ": ", ex);
        }
        return false;
    }

    @Lock(LockType.READ)
    public Contacto getContacto(Long contactoId) {
        return contactoDAO.getContacto(contactoId);
    }

    @Lock(LockType.READ)
    public List<Contacto> getContactos() {
        return contactoDAO.getContactos();
    }

    @Override
    public String contactoList() {
        List<Contacto> contactos = this.getContactos();
        String contactosList = "";
        for (Contacto u : contactos) {
            contactosList += u.toString() + "\n";
        }
        return contactosList;
    }
}
