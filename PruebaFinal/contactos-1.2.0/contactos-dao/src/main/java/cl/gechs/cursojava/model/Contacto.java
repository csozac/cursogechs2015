package cl.gechs.cursojava.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 *
 * @author Carlos Soza Canales <carlos.soza at gmail dot com>
 */
@Entity
@Table(name = "contacto", uniqueConstraints = @UniqueConstraint(columnNames = "contacto_id"))
public class Contacto implements Serializable {

    private Long id;
    private String nombre;
    private String email;
    private String direccion;
    private String telefono;

    @Id
    @GeneratedValue
    @Basic(optional = false)
    @Column(name = "contacto_id")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Basic(optional = false)
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic(optional = false)
    @Column(name = "email")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Basic(optional = false)
    @Column(name = "direccion")
    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    @Basic(optional = false)
    @Column(name = "telefono")
    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    @Override
    public String toString() {
        return id + " | " + nombre + " | " + email + " | ";
    }
}
