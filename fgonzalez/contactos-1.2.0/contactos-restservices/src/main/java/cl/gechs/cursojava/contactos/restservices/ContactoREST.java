package cl.gechs.cursojava.contactos.restservices;

import cl.gechs.cursojava.model.Contacto;
import cl.gechs.cursojava.service.ContactoService;
import cl.gechs.cursojava.service.utils.ServicesDirectory;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import java.util.List;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import org.apache.log4j.Logger;

/**
 * URL: http://localhost:8080/contactos-restservices/contacto/contactosList
 *
 * @author Carlos Soza Canales <carlos.soza at gmail dot com>
 */
@Path("/contacto")
public class ContactoREST {

    private static final Logger LOGGER = Logger.getLogger(ContactoREST.class);
    private static final ContactoService CONTACTO_SERVICE;

    static {
        CONTACTO_SERVICE = ServicesDirectory.getContactoService();
    }

    @GET
    @Path("/contactosList")
    @Produces("application/json")
    public Response obtenerUsuarios() {
        LOGGER.info("Calling method obtenerUsuarios");
        JsonObject output = new JsonObject();
        List<Contacto> users = CONTACTO_SERVICE.getContactos();
        JsonArray userArray = new JsonArray();
        for (Contacto u : users) {
            JsonObject uj = new JsonObject();
            uj.addProperty("nombre", u.getNombre());
            uj.addProperty("email", u.getEmail());
            uj.addProperty("direccion", u.getDireccion());
            uj.addProperty("telefono", u.getTelefono());
            userArray.add(uj);
        }
        output.add("contacto_list", userArray);
        return Response.ok(output.toString()).build();
    }

    @GET
    @Path("/obtener/{contactoId}")
    @Produces("application/json")
    public Response obtenerUsuario(@PathParam("contactoId") Long contactoId) {
        LOGGER.info("Calling method obtenerUsuario");
        JsonObject output = new JsonObject();
        Contacto u = CONTACTO_SERVICE.getContacto(contactoId);
        JsonObject uj = new JsonObject();
        uj.addProperty("nombre", u.getNombre());
        uj.addProperty("email", u.getEmail());
        uj.addProperty("direccion", u.getDireccion());
        uj.addProperty("telefono", u.getTelefono());
        output.add("contacto", uj);
        return Response.ok(output.toString()).build();
    }
    
    
    @GET
    @Path("/contactosListEmpresa/{empresa}")
    @Produces("application/json")
    public Response obtenerUsuariosEmpresa(@PathParam("empresa") String empresa) {
        LOGGER.info("Calling method obtenerUsuariosEmpresa");
        JsonObject output = new JsonObject();
        List<Contacto> users = CONTACTO_SERVICE.getContactosPorEmpresa(empresa);
        JsonArray userArray = new JsonArray();
        for (Contacto u : users) {
            JsonObject uj = new JsonObject();
            uj.addProperty("nombre", u.getNombre());
            uj.addProperty("email", u.getEmail());
            uj.addProperty("direccion", u.getDireccion());
            uj.addProperty("telefono", u.getTelefono());
            uj.addProperty("empresa", u.getEmpresa());
            userArray.add(uj);
        }
        output.add("contacto_list", userArray);
        return Response.ok(output.toString()).build();
    }
}
