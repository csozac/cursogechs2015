package cl.gechs.cursojava;

import cl.gechs.cursojava.dao.ContactoDAO;
import cl.gechs.cursojava.model.Contacto;
import javax.inject.Inject;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.container.test.api.OperateOnDeployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.junit.InSequence;
import org.jboss.shrinkwrap.api.ArchivePaths;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 *
 * @author Carlos Soza C <carlos.soza at gmail dot>
 */
@RunWith(Arquillian.class)
public class ContactoDAOTest {

    @Inject
    private ContactoDAO userDAO;

    @Deployment(name = "contactos-dao", order = 1, testable = true)
    public static WebArchive createDatabaseDeployment() {
        WebArchive war = ShrinkWrap.create(WebArchive.class, "contactos-dao.war")
                .addPackage(ContactoDAO.class.getPackage())
                .addPackage(Contacto.class.getPackage())
                .addAsResource("persistence.xml", "META-INF/persistence.xml")
                .addAsWebInfResource(EmptyAsset.INSTANCE, ArchivePaths.create("beans.xml"));
        System.out.println(war.toString(true));
        return war;
    }

    @Test
    @OperateOnDeployment("contactos-dao")
    @InSequence(1)
    public void testUserDAOAddUpdate() {
        Assert.assertNotNull(this.userDAO);

        Contacto contacto = new Contacto();
        contacto.setNombre("Carlos Soza");
        contacto.setEmail("carlos.soza@gmail.com");
        contacto.setDireccion("Santiago Chile");
        contacto.setTelefono("+5633333");
        contacto.setEmpresa("Su empresa");

        //insert
        Assert.assertTrue(this.userDAO.save(contacto));

        Contacto contacto2 = new Contacto();
        contacto2.setNombre("Franco Gonzalez");
        contacto2.setEmail("franco.gonzalez@rhiscom.com");
        contacto2.setDireccion("Santiago Chile");
        contacto2.setTelefono("+5627132400");
        contacto2.setEmpresa("Rhiscom");

        //insert
        Assert.assertTrue(this.userDAO.save(contacto2));
        
        
        //update
        //contacto.setTelefono("+562223332");
        //Assert.assertTrue(this.userDAO.update(contacto));

        //delete
        //Assert.assertTrue(this.userDAO.delete(contacto));
    }
}
