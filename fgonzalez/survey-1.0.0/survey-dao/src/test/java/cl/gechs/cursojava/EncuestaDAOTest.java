/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.gechs.cursojava;

import cl.gechs.cursojava.dao.jpa.EncuestaDAO;
import cl.gechs.cursojava.dao.jpa.PreguntaDAO;
import cl.gechs.cursojava.model.Encuesta;
import cl.gechs.cursojava.model.Pregunta;
import java.util.Calendar;
import javax.inject.Inject;
import org.apache.log4j.Logger;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.container.test.api.OperateOnDeployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.junit.InSequence;
import org.jboss.shrinkwrap.api.ArchivePaths;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 *
 * @author rhiscom
 */
@RunWith(Arquillian.class)
public class EncuestaDAOTest {
    
    private static final Logger LOGGER = Logger.getLogger(EncuestaDAOTest.class);

    @Inject
    private EncuestaDAO encuestaDAO;
    
    @Inject
    private PreguntaDAO preguntaDAO;
    
    
    @Deployment(name = "survey-database", order = 1, testable = true)
    public static WebArchive createDatabaseDeployment() {
        WebArchive war = ShrinkWrap.create(WebArchive.class, "survey-database.war")
                .addPackage(EncuestaDAO.class.getPackage())
                .addPackage(Encuesta.class.getPackage())
                .addPackage(PreguntaDAO.class.getPackage())
                .addPackage(Pregunta.class.getPackage())
                .addAsWebInfResource("META-INF/jboss-deployment-structure.xml")
                .addAsResource("persistence.xml", "META-INF/persistence.xml")
                .addAsWebInfResource(EmptyAsset.INSTANCE, ArchivePaths.create("beans.xml"));

        return war;
    }
    
    
    @Test
    @OperateOnDeployment("survey-database")
    @InSequence(1)
    public void testInjections() throws Exception {
        Assert.assertNotNull(this.encuestaDAO);
        Assert.assertNotNull(this.preguntaDAO);
    }
    
    
    @Test
    @OperateOnDeployment("survey-database")
    @InSequence(2)
    public void testUserDAOAddUpdate() {
        Assert.assertNotNull(this.encuestaDAO);

        //Crear Encuesta
        Encuesta encuesta = new Encuesta();
        encuesta.setFecha(Calendar.getInstance().getTimeInMillis());
        encuesta.setNombre("Encuesta 1");
        encuesta.setDescripcion("Descripcion");
        Assert.assertTrue(this.encuestaDAO.save(encuesta));
        
//        User user = new User();
//        //UserRol userRolByName = userDAO.getUserRolByName(UserRolEnum.PARTICIPANTE);
//        user.setUserRol(ur);
//        user.setName("Carlos Soza");
//        user.setPassword("csozac");
//        user.setEmail("carlos.soza@gmail.com");
//        user.setUsername("csozac");
//        user.setEnabled(true);
//
//        //insert
//        Assert.assertTrue("Error al guardar el usuario", this.userDAO.save(user));
//        LOGGER.info("Usuario Insertado en BD: " + this.userDAO.getById(user.getId()));
//
//        //update
//        user.setEnabled(Boolean.FALSE);
//        user.setPassword("12345");
//        Assert.assertTrue(this.userDAO.update(user));
//        LOGGER.info("Usuario Actualiazdo en BD: " + this.userDAO.getById(user.getId()));
//
////        //delete
//        Assert.assertTrue(this.userDAO.delete(user));
//        Assert.assertTrue(this.userRolDAO.delete(ur));

    }
    
}
