/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.gechs.cursojava.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 *
 * @author rhiscom
 */

@Entity
@Table(name = "pregunta", uniqueConstraints = @UniqueConstraint(columnNames = "id_Pregunta"))
public class Pregunta implements Serializable {
    
        
    private Long id;
    private String pregunta;

    @Id
    @Basic(optional = false)
    @Column(name = "id_Pregunta", updatable = false)
    @SequenceGenerator(name="pregunta_seq",
                       sequenceName="pregunta_seq",
                       allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="pregunta_seq")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Basic(optional = false)
    @Column(name = "pregunta")
    public String getPregunta() {
        return pregunta;
    }

    public void setPregunta(String pregunta) {
        this.pregunta = pregunta;
    }

    
}
