/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.gechs.cursojava.dao.jpa;

import cl.gechs.cursojava.model.Encuesta;

/**
 *
 * @author rhiscom
 */
@javax.ejb.Stateless
@javax.ejb.LocalBean
public class EncuestaDAO  extends BaseDAO<Encuesta>{

    @Override
    public Class<Encuesta> getObjectClass() {
        return Encuesta.class;
    }
    
}
