/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.gechs.cursojava.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 *
 * @author rhiscom
 */

@Entity
@Table(name = "encuesta", uniqueConstraints = @UniqueConstraint(columnNames = "id_Encuesta"))
public class Encuesta  implements Serializable {
    
    
    private Long id_Encuesta;
    private String nombre;
    private String descripcion;
    private long fecha;
    private List<Pregunta> preguntas;

    
    
    @Id
    @Basic(optional = false)
    @Column(name = "id_Encuesta", updatable = false)
    @SequenceGenerator(name="encuesta_seq",sequenceName="encuesta_seq",allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE,generator="encuesta_seq")
    public Long getId_Encuesta() {
        return id_Encuesta;
    }
    
    public void setId_Encuesta(Long id) {
        this.id_Encuesta = id;
    }

    @Basic(optional = false)
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

   
    @Basic(optional = false)
    @Column(name = "descripcion")
    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Basic(optional = false)
    @Column(name = "fecha")
    public long getFecha() {
        return fecha;
    }

    public void setFecha(long fecha) {
        this.fecha = fecha;
    }
    
    @OneToMany(cascade= CascadeType.ALL, orphanRemoval=true)
    @JoinColumn(name="id_Encuesta")
    public List<Pregunta> getPreguntas() {
        return preguntas;
    }

    public void setPreguntas(List<Pregunta> preguntas) {
        this.preguntas = preguntas;
    }
    
}
