/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.gechs.cursojava.dao.jpa;

import cl.gechs.cursojava.model.Pregunta;

/**
 *
 * @author rhiscom
 */
@javax.ejb.Stateless
@javax.ejb.LocalBean
public class PreguntaDAO  extends BaseDAO<Pregunta> {

    @Override
    public Class<Pregunta> getObjectClass() { 
        return Pregunta.class;
       
    }
    
}
