/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.gechs.export;

import org.apache.log4j.Logger;

/**
 *
 * @author rhiscom
 */
public class ExportToExcel {
    
    private static final Logger LOGGER = Logger.getLogger(ExportToExcel.class);

    public static void main(String[] args) {
        LOGGER.info("Generando Archivo");
        GeneraExcel generaExcel = new GeneraExcel();
        generaExcel.generaExcel("C:\\Users\\rhiscom\\Downloads\\Test.xls");
        LOGGER.info("Archivo Generado con Éxito");
    }
    
}
