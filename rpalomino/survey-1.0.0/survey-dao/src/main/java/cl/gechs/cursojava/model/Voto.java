package cl.gechs.cursojava.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 *
 * @author Carlos Soza Canales <carlos.soza at profondos.com>
 */
@Entity
@Table(name = "voto", uniqueConstraints = @UniqueConstraint(columnNames = "voto_id"))
public class Voto implements Serializable {

    //id
    private Long votoId;
    //Encuesta
    private Encuesta encuesta;
    //Pregunta
    private Pregunta pregunta;
    //ALternatica
    private Alternativa alternativa;
    //PArticpante
    private User participante;
    //Votacion
    private Votacion votacion;

    @Id
    @GeneratedValue
    @Basic(optional = false)
    @Column(name = "voto_id")
    public Long getVotoId() {
        return votoId;
    }

    public void setVotoId(Long votoId) {
        this.votoId = votoId;
    }

    @Basic(optional = false)
    @JoinColumn(name = "encuesta_id", referencedColumnName = "encuesta_id")
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    public Encuesta getEncuesta() {
        return encuesta;
    }

    public void setEncuesta(Encuesta encuesta) {
        this.encuesta = encuesta;
    }

    @Basic(optional = false)
    @JoinColumn(name = "pregunta_id", referencedColumnName = "pregunta_id")
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    public Pregunta getPregunta() {
        return pregunta;
    }

    public void setPregunta(Pregunta pregunta) {
        this.pregunta = pregunta;
    }

    @Basic(optional = false)
    @JoinColumn(name = "alternativa_id", referencedColumnName = "alternativa_id")
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    public Alternativa getAlternativa() {
        return alternativa;
    }

    public void setAlternativa(Alternativa alternativa) {
        this.alternativa = alternativa;
    }

    @Basic(optional = false)
    @JoinColumn(name = "user_id", referencedColumnName = "user_id")
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    public User getParticipante() {
        return participante;
    }

    public void setParticipante(User participante) {
        this.participante = participante;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "votacion_id", nullable = false)
    public Votacion getVotacion() {
        return this.votacion;
    }

    public void setVotacion(Votacion votacion) {
        this.votacion = votacion;
    }

}
