package cl.gechs.cursojava.survey;

import cl.gechs.cursojava.model.User;
import cl.gechs.cursojava.service.UsersService;
import cl.gechs.cursojava.service.utils.ServicesDirectory;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean(name = "usersBean")
@ViewScoped
public class UsersBean implements Serializable {

    private static final UsersService USERS_SERVICE;
    private static final long serialVersionUID = 1L;

    static {
        USERS_SERVICE = ServicesDirectory.getUserService();
    }

    private List<User> users;

    @PostConstruct
    public void init() {
        users = USERS_SERVICE.getUsers();
        System.out.println("List Users Size: " + users.size());
    }

    public List<User> getUsers() {
        return users;
    }
}
