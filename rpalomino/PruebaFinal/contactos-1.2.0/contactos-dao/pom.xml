<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <parent>
        <groupId>cl.gechs.cursojava</groupId>
        <artifactId>contactos</artifactId>
        <version>1.2.0-SNAPSHOT</version>
    </parent>

    <groupId>cl.gechs.cursojava</groupId>
    <version>1.2.0-SNAPSHOT</version>
    <artifactId>contactos-dao</artifactId>
    <packaging>ejb</packaging>

    <name>Contactos DAO</name>

    <url>http://jboss.org/jbossas</url>
    <licenses>
        <license>
            <name>Apache License, Version 2.0</name>
            <distribution>repo</distribution>
            <url>http://www.apache.org/licenses/LICENSE-2.0.html</url>
        </license>
    </licenses>

    <dependencies>
        
        <!-- LOG4J -->
        <dependency>
            <groupId>log4j</groupId>
            <artifactId>log4j</artifactId>
            <version>1.2.15</version>
            <exclusions>
                <exclusion>
                    <groupId>com.sun.jmx</groupId>
                    <artifactId>jmxri</artifactId>
                </exclusion>
                <exclusion>
                    <groupId>com.sun.jdmk</groupId>
                    <artifactId>jmxtools</artifactId>
                </exclusion>
                <exclusion>
                    <groupId>javax.jms</groupId>
                    <artifactId>jms</artifactId>
                </exclusion>
            </exclusions>
            <scope>provided</scope>
        </dependency>
        
        <dependency>
            <groupId>org.jboss.spec.javax.ejb</groupId>
            <artifactId>jboss-ejb-api_3.2_spec</artifactId>
            <scope>provided</scope>
        </dependency>
 
        <dependency>
            <groupId>org.hibernate.javax.persistence</groupId>
            <artifactId>hibernate-jpa-2.1-api</artifactId>
            <scope>provided</scope>
        </dependency>
 
        <!-- Import the CDI API -->
        <dependency>
            <groupId>javax.enterprise</groupId>
            <artifactId>cdi-api</artifactId>
            <scope>provided</scope>
        </dependency>
        <!-- Import the Common Annotations API (JSR-250) -->
        <dependency>
            <groupId>org.jboss.spec.javax.annotation</groupId>
            <artifactId>jboss-annotations-api_1.2_spec</artifactId>
            <scope>provided</scope>
        </dependency>
        
        <!-- JUNIT & ARQUILLIAN -->
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.jboss.arquillian.junit</groupId>
            <artifactId>arquillian-junit-container</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.jboss.shrinkwrap.resolver</groupId>
            <artifactId>shrinkwrap-resolver-depchain</artifactId>
            <scope>test</scope>
            <type>pom</type>
        </dependency>
        <dependency>
            <groupId>org.jboss.shrinkwrap.descriptors</groupId>
            <artifactId>shrinkwrap-descriptors-impl-javaee</artifactId>
            <scope>test</scope>
            <exclusions>
                <!-- This exclusion is needed to be able to setup the project in Windows:
                it otherwise includes transitive dependency to the JDK JConsole -->
                <exclusion>
                    <artifactId>wildfly-patching</artifactId>
                    <groupId>org.wildfly</groupId>
                </exclusion>
            </exclusions>
        </dependency>
        <dependency>
            <groupId>org.jboss.arquillian.extension</groupId>
            <artifactId>arquillian-jacoco</artifactId>
            <scope>test</scope>
            <version>${version.arquillian.jacoco}</version>
        </dependency>
        <dependency>
            <groupId>org.jacoco</groupId>
            <artifactId>org.jacoco.core</artifactId>
            <version>${version.jacoco}</version>
        </dependency>
        
    </dependencies>

    <build>
        <finalName>contactos-dao</finalName>
        <plugins>
            <plugin>
                <artifactId>maven-ejb-plugin</artifactId>
                <version>${version.ejb.plugin}</version>
                <configuration>
                    <!-- Tell Maven we are using EJB 3.1 -->
                    <ejbVersion>3.1</ejbVersion>
                </configuration>
            </plugin>
        </plugins>
    </build>

    <profiles>
        <profile>
            <!-- The default profile skips all tests, though you can tune it 
            to run just unit tests based on a custom pattern -->
            <!-- Seperate profiles are provided for running all tests, including 
            Arquillian tests that execute in the specified container -->
            <id>default</id>
            <activation>
                <activeByDefault>true</activeByDefault>
            </activation>
            <build>
                <plugins>
                    <plugin>
                        <artifactId>maven-surefire-plugin</artifactId>
                        <version>${version.surefire.plugin}</version>
                        <configuration>
                            <skip>true</skip>
                            <systemProperties>
                                <property>
                                    <name>projectVersion</name>
                                    <value>${project.version}</value>
                                </property>
                            </systemProperties>
                        </configuration>
                    </plugin>
                </plugins>
            </build>
        </profile>
        
        <profile>
            <id>integration-tests</id>
            <activation>
                <activeByDefault>false</activeByDefault>
            </activation>
            
            <!-- the dependencies for Wildfly  -->
            <dependencies>
                <dependency>
                    <groupId>org.wildfly</groupId>
                    <artifactId>wildfly-arquillian-container-managed</artifactId>
                    <version>${version.wildfly}</version>
                    <scope>test</scope>
                    <exclusions>
                        <!-- This exclusion is needed to be able to setup the project in Windows:
                        it otherwise includes transitive dependency to the JDK JConsole -->
                        <exclusion>
                            <artifactId>wildfly-patching</artifactId>
                            <groupId>org.wildfly</groupId>
                        </exclusion>
                    </exclusions>
                </dependency>
            </dependencies>
 
            <build>
                <plugins>
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-deploy-plugin</artifactId>
                        <version>2.8</version>
                        <configuration>
                            <skip>true</skip>
                        </configuration>
                    </plugin>
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-surefire-plugin</artifactId>
                        <version>2.17</version>
                        <configuration>
                            <systemPropertyVariables>
                                <projectVersion>${project.version}</projectVersion>
                                <databaseEngine>mysql</databaseEngine>
                            </systemPropertyVariables>
                            <testFailureIgnore>true</testFailureIgnore>
                        </configuration>
                    </plugin>
                    <plugin>
                        <groupId>org.jacoco</groupId>
                        <artifactId>jacoco-maven-plugin</artifactId>
                        <version>${version.jacoco}</version>
                        <executions>
                            <execution>
                                <goals>
                                    <goal>prepare-agent</goal>
                                </goals>
                            </execution>
                            <execution>
                                <id>report</id>
                                <phase>prepare-package</phase>
                                <goals>
                                    <goal>report</goal>
                                </goals>
                            </execution>
                        </executions>
                    </plugin>
                    <plugin>
                        <artifactId>maven-dependency-plugin</artifactId>
                        <executions>
                            <execution>
                                <id>unpack</id>
                                <phase>process-test-classes</phase>
                                <goals>
                                    <goal>unpack</goal>
                                </goals>
                                <configuration>
                                    <artifactItems>
                                        <artifactItem>
                                            <groupId>org.wildfly</groupId>
                                            <artifactId>wildfly-dist</artifactId>
                                            <version>${version.wildfly}</version>
                                            <type>zip</type>
                                            <overWrite>true</overWrite>
                                            <outputDirectory>${project.build.directory}</outputDirectory>
                                        </artifactItem>
                                    </artifactItems>
                                </configuration>
                            </execution>
                        </executions>
                    </plugin>
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-resources-plugin</artifactId>
                        <version>2.7</version>
                        <executions>
                            <!--Copy the AS configuration files so we can use our custom configurations--> 
                            <execution>
                                <id>configure-as</id>
                                <phase>process-test-classes</phase>
                                <goals>
                                    <goal>copy-resources</goal>
                                </goals>
                                <configuration>
                                    <outputDirectory>${project.build.directory}/wildfly-${version.wildfly}/standalone/configuration</outputDirectory>
                                    <overwrite>true</overwrite>
                                    <resources>
                                        <resource>
                                            <directory>../config-files/standalone/configuration</directory>
                                        </resource>
                                    </resources>
                                </configuration>
                            </execution>
                            <!--Copy the AS configuration modules so we can use our custom configurations--> 
                            <execution>
                                <id>configure-modules</id>
                                <phase>process-test-classes</phase>
                                <goals>
                                    <goal>copy-resources</goal>
                                </goals>
                                <configuration>
                                    <outputDirectory>${project.build.directory}/wildfly-${version.wildfly}/modules/system/layers/base/</outputDirectory>
                                    <overwrite>true</overwrite>
                                    <resources>
                                        <resource>
                                            <directory>../config-files/modules/</directory>
                                        </resource>
                                    </resources>
                                </configuration>
                            </execution>
                        </executions>
                    </plugin>    
                </plugins>
            </build>
        </profile>
    </profiles>
</project>
