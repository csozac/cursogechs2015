package cl.gechs.cursojava.service.utils;

import cl.gechs.cursojava.service.ContactoService;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import org.apache.log4j.Logger;

/**
 *
 * @author David Gutiérrez <david.gutierrez at profondos.com>
 */
public class ServicesDirectory {

    private static final Logger LOGGER = Logger.getLogger(ServicesDirectory.class);
    // services
    private static transient ContactoService contactoService;

    private ServicesDirectory() {
    }

    public static ContactoService getContactoService() {
        try {
            if (contactoService == null) {
                contactoService = (ContactoService) InitialContext
                        .doLookup("java:global/contactos-ear/contactos-service/ContactoService!cl.gechs.cursojava.service.ContactoService");
            }
        } catch (NamingException ex) {
            LOGGER.warn("Error injecting services: " + ex.getMessage(), ex);
        }
        return contactoService;
    }
}
