package cl.gechs.cursojava.contactos.servlets;

import cl.gechs.cursojava.model.Contacto;
import cl.gechs.cursojava.service.ContactoService;
import cl.gechs.cursojava.service.utils.ServicesDirectory;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;

/**
 *
 * @author Carlos Soza Canales <carlos.soza at gmail dot com>
 */
@WebServlet("/servlets/contactos/ContactosPorEmpresaServlet")
public class ContactosPorEmpresaServlet extends HttpServlet {

    private static final Logger LOGGER = Logger.getLogger(ContactosPorEmpresaServlet.class);
    private static final long serialVersionUID = 1L;
    private static final ContactoService CONTACTOS_SERVICE;

    static {
        CONTACTOS_SERVICE = ServicesDirectory.getContactoService();
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json;charset=UTF-8");
        JsonObject output = new JsonObject();
        PrintWriter out = response.getWriter();

        try {
           String parametro1 = request.getParameter("empresa");
           List<Contacto> contactos = CONTACTOS_SERVICE.getContactosPorEmpresa(parametro1);
           JsonArray userArray = new JsonArray();
           for (Contacto c : contactos) {
              JsonObject cj = new JsonObject();
              cj.addProperty("nombre", c.getNombre());
              cj.addProperty("email", c.getEmail());
              cj.addProperty("direccion", c.getDireccion());
              cj.addProperty("telefono", c.getTelefono());
              userArray.add(cj);
           }
           output.add("contacto_list", userArray);
        } catch (Exception ex) {
            LOGGER.error("Cannot get Contacto List: ", ex);
            output.addProperty("result", "ERROR");
            output.addProperty("exception", ex.toString());
        } finally {
            out.print(output.toString());
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
