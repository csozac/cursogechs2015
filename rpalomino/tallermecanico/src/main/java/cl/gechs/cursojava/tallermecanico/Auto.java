package cl.gechs.cursojava.tallermecanico;

/**
 *
 * @author csozac
 */
public class Auto
{

    private String marca;
    private String modelo;
    private String color;
    private int numeroDePuertas;
    private String nivelDeAceite;
    private String nivelDeAgua;
    private int kilometraje;

    /**
     * Constructor Vacio
     */
    Auto()
    {
    }

    /**
     * Constructor para setear los valores de los atributos de los objetos instanciados de la clase Auto.
     * @param marca
     * @param modelo
     * @param color
     * @param numeroDePuertas
     * @param nivelDeAceite
     * @param nivelDeAgua
     * @param kilometraje
     */
    public Auto(String marca, String modelo, String color, int numeroDePuertas, String nivelDeAceite, String nivelDeAgua, int kilometraje)
    {
        this.marca = marca;
        this.modelo = modelo;
        this.color = color;
        this.numeroDePuertas = numeroDePuertas;
        this.nivelDeAceite = nivelDeAceite;
        this.nivelDeAgua = nivelDeAgua;
        this.kilometraje = kilometraje;
    }

    public int getKilometraje()
    {
        return kilometraje;
    }

    public void setKilometraje(int kilometraje)
    {
        this.kilometraje = kilometraje;
    }

    public String getColor()
    {
        return color;
    }

    public void setColor(String color)
    {
        this.color = color;
    }

    public String getMarca()
    {
        return marca;
    }

    public void setMarca(String marca)
    {
        this.marca = marca;
    }

    public String getModelo()
    {
        return modelo;
    }

    public void setModelo(String modelo)
    {
        this.modelo = modelo;
    }

    public String getNivelDeAceite()
    {
        return nivelDeAceite;
    }

    public void setNivelDeAceite(String nivelDeAceite)
    {
        this.nivelDeAceite = nivelDeAceite;
    }

    public String getNivelDeAgua()
    {
        return nivelDeAgua;
    }

    public void setNivelDeAgua(String nivelDeAgua)
    {
        this.nivelDeAgua = nivelDeAgua;
    }

    public int getNumeroDePuertas()
    {
        return numeroDePuertas;
    }

    public void setNumeroDePuertas(int numeroDePuertas)
    {
        this.numeroDePuertas = numeroDePuertas;
    }
}
