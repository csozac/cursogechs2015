/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.gechs.cursojava.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 *
 * @author RafaelPalominos
 */
@Entity
@Table(name = "Voto", uniqueConstraints =
@UniqueConstraint(columnNames = "idVoto"))
public class Vote implements Serializable {

   private static final long serialVersionUID = 1L;
   @Id
   @GeneratedValue
   @Basic(optional = false)
   @Column(name = "idVoto")
   private Long id;
   
   @Basic(optional = false)
   @JoinColumn(name = "idEncuesta", referencedColumnName = "id")
   @OneToOne(mappedBy="Encuesta",fetch= FetchType.EAGER)
   private Survey survey;
   
   @Basic(optional = false)
   @JoinColumn(name = "idVotacion", referencedColumnName = "id")
   @ManyToOne( fetch = FetchType.EAGER)
   private Votation votation;
   
   @Basic(optional = false)
   @JoinColumn(name = "idPregunta", referencedColumnName = "id")
   @OneToOne(mappedBy="Pregunta",fetch= FetchType.EAGER)
   private Question question;
   
   @Basic(optional = false)
   @JoinColumn(name = "idVotante", referencedColumnName = "id")
   @OneToOne(mappedBy="Votante",fetch= FetchType.EAGER)
   private Voter voter;

   public Long getId() {
      return id;
   }

   public void setId(Long id) {
      this.id = id;
   }

   public Question getQuestion() {
      return question;
   }

   public void setQuestion(Question question) {
      this.question = question;
   }

   public Survey getSurvey() {
      return survey;
   }

   public void setSurvey(Survey survey) {
      this.survey = survey;
   }

   public Votation getVotation() {
      return votation;
   }

   public void setVotation(Votation votation) {
      this.votation = votation;
   }

   public Voter getVoter() {
      return voter;
   }

   public void setVoter(Voter voter) {
      this.voter = voter;
   }
   
}
