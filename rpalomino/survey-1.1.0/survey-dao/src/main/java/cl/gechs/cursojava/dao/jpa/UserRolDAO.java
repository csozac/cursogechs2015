package cl.gechs.cursojava.dao.jpa;

import cl.gechs.cursojava.model.UserRol;
import cl.gechs.cursojava.model.UserRolEnum;
import java.util.List;
import javax.persistence.Query;
import org.apache.log4j.Logger;

/**
 *
 * @author Carlos Soza Canales <carlos.soza at gmail dot com>
 */
@javax.ejb.Stateless
@javax.ejb.LocalBean
public class UserRolDAO extends BaseDAO<UserRol> {

    private static final Logger LOGGER = Logger.getLogger(UserRolDAO.class);

    @Override
    public Class<UserRol> getObjectClass() {
        return UserRol.class;
    }

    public List<UserRol> getUserRoles() {
        Query query = this.getEntityManager().createQuery("FROM UserRol");
        try {
            return (List<UserRol>) query.getResultList();
        } catch (javax.persistence.NoResultException ex) {
            return null;
        } catch (Exception ex) {
            LOGGER.error("ERROR DAO: ", ex);
            return null;
        }
    }

    public UserRol getUserRolByName(UserRolEnum rol) {

        Query query = this.getEntityManager().createQuery("FROM UserRol rol WHERE rol.nombre=:rol");
        query.setParameter("rol", rol);
        query.setMaxResults(1);
        try {
            return (UserRol) query.getSingleResult();
        } catch (javax.persistence.NoResultException ex) {
            return null;
        } catch (Exception ex) {
            LOGGER.error("ERROR DAO: ", ex);
            return null;
        }
    }
}
