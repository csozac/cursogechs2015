/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.gechs.cursojava.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.UniqueConstraint;

/**
 *
 * @author RafaelPalominos
 */
@Entity
@Table(name = "Encuesta", uniqueConstraints =
@UniqueConstraint(columnNames = "idEncuesta"))
public class Survey implements Serializable {

   private static final long serialVersionUID = 1L;
   @Id
   @GeneratedValue
   @Basic(optional = false)
   @Column(name = "idEncuesta")
   private Long id;
   @Basic(optional = false)
   @Column(name = "nombre")
   private String name;
   @Basic(optional = true)
   @Column(name = "descripcion")
   private String description;
   @Basic(optional = false)
   @Temporal(javax.persistence.TemporalType.DATE)
   @Column(name = "fecha")
   private Date surveyDate;
   
   @Basic(optional = false)
   @JoinColumn(name = "idPregunta", referencedColumnName = "id")
   @OneToMany(fetch = FetchType.EAGER)
   private List<Question> questionList;
   
   @Basic(optional = false)
   @JoinColumn(name = "idVotacion", referencedColumnName = "id")
   @OneToOne(optional = false, fetch = FetchType.LAZY)
   private Votation votation;
   
   @Basic(optional = false)
   @JoinColumn(name = "idVoto", referencedColumnName = "id")
   @OneToOne(optional = false, fetch = FetchType.LAZY)
   private Vote vote;

   public Long getId() {
      return id;
   }

   public void setId(Long id) {
      this.id = id;
   }

   public String getDescription() {
      return description;
   }

   public void setDescription(String description) {
      this.description = description;
   }

   public String getName() {
      return name;
   }

   public void setName(String name) {
      this.name = name;
   }

   public Date getSurveyDate() {
      return surveyDate;
   }

   public void setSurveyDate(Date surveyDate) {
      this.surveyDate = surveyDate;
   }

   public List<Question> getQuestionList() {
      return questionList;
   }

   public void setQuestionList(List<Question> questionList) {
      this.questionList = questionList;
   }

   public Votation getVotation() {
      return votation;
   }

   public void setVotation(Votation votation) {
      this.votation = votation;
   }

   public Vote getVote() {
      return vote;
   }

   public void setVote(Vote vote) {
      this.vote = vote;
   }
   
}
