/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.gechs.cursojava.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 *
 * @author RafaelPalominos
 */
@Entity
@Table(name = "Pregunta", uniqueConstraints =
@UniqueConstraint(columnNames = "idPregunta"))
public class Question implements Serializable {

   private static final long serialVersionUID = 1L;
   @Id
   @GeneratedValue
   @Basic(optional = false)
   @Column(name = "idPregunta")
   private Long id;
   @Basic(optional = false)
   @Column(name = "pregunta")
   private String question;
   
   @Basic(optional = false)
   @JoinColumn(name = "idOpcion", referencedColumnName = "id")
   @OneToMany( fetch = FetchType.EAGER)
   private List<Option> optionList;
   
   @Basic(optional = false)
   @JoinColumn(name = "idEncuesta", referencedColumnName = "id")
   @ManyToOne( fetch = FetchType.EAGER)
   private Survey survey;
   
   @Basic(optional = false)
   @JoinColumn(name = "idVoto", referencedColumnName = "id")
   @OneToOne(mappedBy="Voto",fetch= FetchType.EAGER)
   private Vote vote;

   public Long getId() {
      return id;
   }

   public void setId(Long id) {
      this.id = id;
   }

   public String getQuestion() {
      return question;
   }

   public void setQuestion(String question) {
      this.question = question;
   }

   public List<Option> getOptionList() {
      return optionList;
   }

   public void setOptionList(List<Option> optionList) {
      this.optionList = optionList;
   }

   public Survey getSurvey() {
      return survey;
   }

   public void setSurvey(Survey survey) {
      this.survey = survey;
   }

   public Vote getVote() {
      return vote;
   }

   public void setVote(Vote vote) {
      this.vote = vote;
   }
   
   
}
