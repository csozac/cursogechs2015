/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.gechs.cursojava.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.UniqueConstraint;

/**
 *
 * @author RafaelPalominos
 */
@Entity
@Table(name = "Votante", uniqueConstraints =
@UniqueConstraint(columnNames = "idVotante"))
public class Voter implements Serializable {

   private static final long serialVersionUID = 1L;
   @Id
   @GeneratedValue
   @Basic(optional = false)
   @Column(name = "idVotante")
   private Long id;
   @Basic(optional = false)
   @Column(name = "nombre")
   private String name;
   @Basic(optional = true)
   @Column(name = "email")
   private String email;

   @Basic(optional = false)
   @JoinColumn(name = "idVoto", referencedColumnName = "id")
   @OneToOne(mappedBy="Voto", fetch = FetchType.EAGER)
   private Vote vote;
   
   public Long getId() {
      return id;
   }

   public void setId(Long id) {
      this.id = id;
   }

   public String getEmail() {
      return email;
   }

   public void setEmail(String email) {
      this.email = email;
   }

   public String getName() {
      return name;
   }

   public void setName(String name) {
      this.name = name;
   }

   public Vote getVote() {
      return vote;
   }

   public void setVote(Vote vote) {
      this.vote = vote;
   }

   
}
