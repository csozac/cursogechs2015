/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.gechs.cursojava.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 *
 * @author RafaelPalominos
 */
@Entity
@Table(name = "Opcion", uniqueConstraints =
@UniqueConstraint(columnNames = "idOpcion"))
public class Option implements Serializable {

   private static final long serialVersionUID = 1L;
   @Id
   @GeneratedValue
   @Basic(optional = false)
   @Column(name = "idOpcion")
   private Long id;
   @Basic(optional = false)
   @Column(name = "alternativa")
   private String option;
   
   @Basic(optional = false)
   @JoinColumn(name = "idPregunta", referencedColumnName = "id")
   @ManyToOne(fetch = FetchType.EAGER)
   private Question question;
   
   @Basic(optional = false)
   @JoinColumn(name = "idVoto", referencedColumnName = "id")
   @OneToOne(mappedBy="Voto",fetch= FetchType.EAGER)
   private Vote vote;
   
   public Long getId() {
      return id;
   }

   public void setId(Long id) {
      this.id = id;
   }

   public String getOption() {
      return option;
   }

   public void setOption(String question) {
      this.option = question;
   }

   public Question getQuestion() {
      return question;
   }

   public void setQuestion(Question question) {
      this.question = question;
   }

   public Vote getVote() {
      return vote;
   }

   public void setVote(Vote vote) {
      this.vote = vote;
   }
   
   
}
