/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.gechs.cursojava.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 *
 * @author RafaelPalominos
 */
@Entity
@Table(name = "Votacion", uniqueConstraints =
@UniqueConstraint(columnNames = "idVotacion"))
public class Votation implements Serializable {

   private static final long serialVersionUID = 1L;
   @Id
   @GeneratedValue
   @Basic(optional = false)
   @Column(name = "idVotacion")
   private Long id;
   
   @Basic(optional = false)
   @JoinColumn(name = "idVoto", referencedColumnName = "id")
   @OneToMany(fetch = FetchType.EAGER)
   private List<Vote> voteList;
   
   @Basic(optional = false)
   @JoinColumn(name = "idEncuesta", referencedColumnName = "id")
   @OneToOne(mappedBy="Encuesta",fetch= FetchType.EAGER)
   private Survey survey;

   public Long getId() {
      return id;
   }

   public void setId(Long id) {
      this.id = id;
   }

   public Survey getSurvey() {
      return survey;
   }

   public void setSurvey(Survey survey) {
      this.survey = survey;
   }

   public List<Vote> getVoteList() {
      return voteList;
   }

   public void setVoteList(List<Vote> voteList) {
      this.voteList = voteList;
   }
   
}
