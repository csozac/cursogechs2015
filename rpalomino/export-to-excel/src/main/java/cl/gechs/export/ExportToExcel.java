/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.gechs.export;

import org.apache.log4j.Logger;

/**
 *
 * @author RafaelPalominos
 */
public class ExportToExcel {

   private static final Logger LOGGER = Logger.getLogger(ExportToExcel.class);

   /**
    * Main file that invokes GeneraExcel
    * @param args the command line arguments
    */

   public static void main(String[] args) {
      LOGGER.info("Generando Archivo");
      GeneraExcel generaExcel = new GeneraExcel();
// Usar Path de archivo de sus máquinas
      generaExcel.generaExcel("/Users/RafaelPalominos/Downloads/Test.xls");
      LOGGER.info("Archivo Generado con Éxito");
   }

}
