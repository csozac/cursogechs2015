/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.gechs.export;

import java.io.File;
import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * Tests ExportToExcel class
 * @author RafaelPalominos
 */
public class ExportToExcelTest {

   private static final Logger LOGGER = Logger.getLogger(ExportToExcelTest.class);

   public ExportToExcelTest() {
   }
/**
 * The BeforeClass annotation indicates that the static method to which is attached must be executed once and before all tests in the class. That happens when the test methods share computationally expensive setup (e.g. connect to database).
 */
   @BeforeClass
   public static void setUpClass() {
      LOGGER.debug("Setup Class");
   }

   @AfterClass
   public static void tearDownClass() {
      LOGGER.debug("Tear Down Class");
   }
/**
 * The Before annotation indicates that this method must be executed before each test in the class, so as to execute some preconditions necessary for the test.
 */
   @Before
   public void setUp() {
      LOGGER.debug("Setup");
   }

   @After
   public void tearDown() {
      LOGGER.debug("tearDown");
   }
/**
 * Test para probar que el archivo existe
 */
   @Test
   public void testGenerarExcel() {
      // dejarlo en un directorio cambiante. el test se espera ejecutar montones de veces.
      String filename = "target/Test.xls"; 
      GeneraExcel generaExcel = new GeneraExcel();
      generaExcel.generaExcel(filename);
      File f = new File(filename);
      Assert.assertTrue("Se espera que el archivo Exista: " + filename, f.exists());
   }

   @Test
   public void testDummy() {
      Assert.assertTrue(true);
   }
}
